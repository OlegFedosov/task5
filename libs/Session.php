<?php

class Session implements iWorkData
{
    public function saveData($key, $val)
    {
        if (!$key || !$val) {
            return false;
        }
        $_SESSION[$key] = $val;
        return true;
    }

    public function getData($key)
    {
        if (!$key or !isset($_SESSION[$key])) {
            return false;
        }
        return $_SESSION[$key];
    }

    public function deleteData($key)
    {
        if (!$key or !isset($_SESSION[$key])) {
            return false;
        }
        unset($_SESSION[$key]);
        return true;    
    }
}
