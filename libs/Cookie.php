<?php

class Cookie implements iWorkData
{
    public function saveData($key, $val)
    {
        if (!$key || !$val) {
            return false;
        }
        if (!isset($_COOKIE[$key]) {
            setcookie($key, $val, time()+3600)
        }
        $_COOKIE[$key] = $val;
        return true;
    }

    public function getData($key)
    {
        if (!$key or !isset($_COOKIE[$key])) {
            return false;
        }
        return $_COOKIE[$key];
    }

    public function deleteData($key)
    {
        if (!$key or !isset($_COOKIE[$key])) {
            return falise;
        }
        unset($_COOKIE[$key]);
        return true;    
    }
}
